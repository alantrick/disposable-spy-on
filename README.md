# Disposable Jest Spy On


## TypeScript 5.2

Using this correctly with NodeJS requires TypeScript 5.2 as well as a
polyfill (see example usage below). I wouldn't recommend it for
production code yet, since it's still in beta an a lot of important
tooling (prettier, eslint, etc) doesn't support it yet.

## Installation

1. Install TypeScript 5.2 (currently `yarn add -D typescript@beta` or
   `npm i -D typescript@beta`).
2. Install this package (`yarn add disposable-spy-on` or `npm i disposable-spy-on`).


## Usage

Here's an example of using it in a jest test.

```ts
import disposableSpyOn from "disposable-spy-on";

// Either add this polyfill yourself, or use something like core-js
// See https://devblogs.microsoft.com/typescript/announcing-typescript-5-2-beta/
// @ts-expect-error
Symbol.dispose ??= Symbol("Symbol.dispose");

const hello = () => console.log("hello world")

describe("hello", () => {
  it("writes to console") => {
    using spy = disposableSpyOn(console, "log").mockImplementation(() => undefined);
    hello()
    expect(spy).toBeCalledTimes(1);
    expect(spy).toBeCalledWith('hello world');
  });
  // console.log is no longer mocked
});
```

## TODO

- ESM support
- Fix prettier (TypeScript 5.2 support in progress: https://github.com/prettier/prettier/issues/15004)
- Fix eslint (TypeScript 5.2 support in progress: https://github.com/typescript-eslint/typescript-eslint/issues/7155)