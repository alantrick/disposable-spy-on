import * as jestMock from "jest-mock";

declare type ResolveType<T extends jestMock.FunctionLike> =
  ReturnType<T> extends PromiseLike<infer U> ? U : never;
declare type RejectType<T extends jestMock.FunctionLike> =
  ReturnType<T> extends PromiseLike<any> ? unknown : never;

type MockInstanceUnwrap<T> = T extends jestMock.MockInstance<infer U>
  ? U
  : never;

interface DisposableSpyOn<T extends jestMock.FunctionLike> extends Disposable {
  spy: jestMock.MockInstance<T>;
  // this is so we can get the jest matchers to work properly
  _isMockFunction: true;
  // getMockImplementation() is not well documented
  getMockName(): string;
  get mock(): jestMock.MockInstance<T>["mock"];
  mockClear(): this;
  mockReset(): this;
  mockImplementation(fn: T): this;
  mockImplementationOnce(fn: T): this;
  // TODO: implement this variation
  // withImplementation(
  //   fn: T,
  //   callback: () => Promise<unknown>
  // ): Promise<void>;
  withImplementation(fn: T, callback: () => void): void;
  mockName(name: string): this;
  // mockReturnThis() doesn't make a whole lot of sense here
  mockReturnValue(value: ReturnType<T>): this;
  mockReturnValueOnce(value: ReturnType<T>): this;
  mockResolvedValue(value: ResolveType<T>): this;
  mockResolvedValueOnce(value: ResolveType<T>): this;
  mockRejectedValue(value: RejectType<T>): this;
  mockRejectedValueOnce(value: RejectType<T>): this;
}

/**
 * Set and automatically revert spy on mocks
 */

function disposableSpyOn<
  T extends object,
  K extends jestMock.PropertyLikeKeys<T>,
  V extends Required<T>[K],
  A extends "get" | "set"
>(
  object: T,
  methodKey: K,
  accessType: A
): A extends "get"
  ? DisposableSpyOn<() => V>
  : A extends "set"
  ? DisposableSpyOn<(v: V) => void>
  : never;

function disposableSpyOn<
  T extends object,
  K extends jestMock.ConstructorLikeKeys<T> | jestMock.MethodLikeKeys<T>,
  V extends Required<T>[K]
>(
  object: T,
  methodKey: K
): V extends jestMock.ClassLike | jestMock.FunctionLike
  ? DisposableSpyOn<MockInstanceUnwrap<jestMock.Spied<V>>>
  : never;

function disposableSpyOn<T extends object>(
  object: T,
  methodKey: keyof T,
  accessType?: "get" | "set"
): DisposableSpyOn<MockInstanceUnwrap<jestMock.MockInstance>> {
  const spy = jestMock.spyOn(object, methodKey as any, accessType as any);

  return {
    _isMockFunction: true,
    [Symbol.dispose]() {
      spy.mockRestore();
    },
    getMockName() {
      return spy.getMockName();
    },
    get mock() {
      return spy.mock;
    },
    mockClear() {
      spy.mockClear();
      return this;
    },
    mockReset() {
      spy.mockReset();
      return this;
    },
    mockImplementation(fn) {
      spy.mockImplementation(fn);
      return this;
    },
    mockImplementationOnce(fn) {
      spy.mockImplementationOnce(fn);
      return this;
    },
    withImplementation(fn, callback) {
      return spy.withImplementation(fn, callback);
    },
    mockName(name) {
      spy.mockName(name);
      return this;
    },
    mockReturnValue(value) {
      spy.mockReturnValue(value as any);
      return this;
    },
    mockReturnValueOnce(value) {
      spy.mockReturnValueOnce(value as any);
      return this;
    },
    mockResolvedValue(value) {
      spy.mockResolvedValue(value);
      return this;
    },
    mockResolvedValueOnce(value) {
      spy.mockResolvedValueOnce(value);
      return this;
    },
    mockRejectedValue(value) {
      spy.mockRejectedValue(value);
      return this;
    },
    mockRejectedValueOnce(value) {
      spy.mockRejectedValueOnce(value);
      return this;
    },
    spy,
  };
}

export default disposableSpyOn;
