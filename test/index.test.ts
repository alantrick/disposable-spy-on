import disposableSpyOn from "../src";

// While typescript is happy to convert the "using" keyword into something
// functional, but does not want to do any actual polyfill-ing.

// In general, it's expected that you might use something like core-js or
// babel to provide polyfills. In this case, however, that's over-kill:
// all we need here is Symbol.dispose, and we can get it by just doing this:
// @ts-expect-error
Symbol.dispose ??= Symbol("Symbol.dispose");
// See https://devblogs.microsoft.com/typescript/announcing-typescript-5-2-beta/

describe("disposableSpyOn", () => {
  const obj = {
    _one: 1,
    get one(): number {
      return this._one;
    },
    set one(value: number) {
      this._one = value;
    },
    return1: () => 1,
    return1Async: () => Promise.resolve(1),
  };
  const hello = () => console.log("hello world")

  it("captures hello world writes to console", () => {
    using spy = disposableSpyOn(console, "log").mockImplementation(() => undefined);
    hello()
    expect(spy).toBeCalledTimes(1);
    expect(spy).toBeCalledWith('hello world');
  });

  it("replaces properties with get till out of scope", () => {
    (() => {
      using spy = disposableSpyOn(obj, "one", "get").mockImplementation(
        () => 2
      );
      expect(obj.one).toBe(2);
      expect(spy).toBeCalledTimes(1);
      expect(obj.one).toBe(2);
      expect(spy).toBeCalledTimes(2);
    })();
    expect(obj.one).toBe(1);
  });

  it("replaces properties with set till out of scope", () => {
    (() => {
      using spy = disposableSpyOn(obj, "one", "set").mockImplementation(
        () => 2
      );
      expect(obj.one).toBe(1);
      expect(spy).toBeCalledTimes(0);
      obj.one = 2;
      expect(obj.one).toBe(1);
      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith(2);
    })();
    expect(obj.one).toBe(1);
  });

  it("replaces methods till out of scope", () => {
    (() => {
      using spy = disposableSpyOn(obj, "return1").mockImplementation(() => 2);
      expect(obj.return1()).toBe(2);
      expect(spy).toBeCalledTimes(1);
      expect(obj.return1()).toBe(2);
      expect(spy).toBeCalledTimes(2);
    })();
    expect(obj.return1()).toBe(1);
  });

  it("stops mockImplementationOnce when out of scope", () => {
    (() => {
      using _ = disposableSpyOn(obj, "return1").mockImplementationOnce(() => 2);
    })();
    expect(obj.return1()).toBe(1);
  });

  it("proxies getMockName()/mockName()", () => {
    using spy = disposableSpyOn(obj, "return1");
    expect(spy.getMockName()).toBe("jest.fn()");
    spy.mockName("spy");
    expect(spy.getMockName()).toBe("spy");
  });

  it("proxies getMockClear", () => {
    using spy = disposableSpyOn(obj, "return1").mockImplementation(() => 2);
    expect(obj.return1()).toBe(2);
    expect(spy).toBeCalledTimes(1);
    spy.mockClear();
    expect(spy).toBeCalledTimes(0);
    expect(obj.return1()).toBe(2);
    expect(spy).toBeCalledTimes(1);
  });

  it("proxies getMockReset", () => {
    using spy = disposableSpyOn(obj, "return1").mockImplementation(() => 2);
    expect(obj.return1()).toBe(2);
    expect(spy).toBeCalledTimes(1);
    spy.mockReset();
    expect(spy).toBeCalledTimes(0);
    expect(obj.return1()).toBe(1);
    expect(spy).toBeCalledTimes(1);
  });

  it("proxies withImplementation", () => {
    using spy = disposableSpyOn(obj, "return1");
    spy.withImplementation(
      () => 2,
      () => {
        expect(obj.return1()).toBe(2);
      }
    );
    expect(spy).toBeCalledTimes(1);
    expect(obj.return1()).toBe(1);
    expect(spy).toBeCalledTimes(2);
  });

  it("proxies mockReturnValue", () => {
    using spy = disposableSpyOn(obj, "return1").mockReturnValue(3);
    expect(obj.return1()).toBe(3);
    expect(spy).toBeCalledTimes(1);
  });

  it("proxies mockReturnValueOnce", () => {
    using spy = disposableSpyOn(obj, "return1").mockReturnValueOnce(3);
    expect(obj.return1()).toBe(3);
    expect(spy).toBeCalledTimes(1);
    expect(obj.return1()).toBe(1);
    expect(spy).toBeCalledTimes(2);
  });

  it("proxies mockResolvedValue", () => {
    using spy = disposableSpyOn(obj, "return1Async").mockResolvedValue(3);
    expect(obj.return1Async()).resolves.toBe(3);
    expect(spy).toBeCalledTimes(1);
  });

  it("proxies mockResolvedValueOnce", () => {
    using spy = disposableSpyOn(obj, "return1Async").mockResolvedValueOnce(3);
    expect(obj.return1Async()).resolves.toBe(3);
    expect(spy).toBeCalledTimes(1);
    expect(obj.return1Async()).resolves.toBe(1);
    expect(spy).toBeCalledTimes(2);
  });

  it("proxies mockRejectedValue", () => {
    using spy = disposableSpyOn(obj, "return1Async").mockRejectedValue(
      Error("3")
    );
    expect(obj.return1Async()).rejects.toThrowError(Error("3"));
    expect(spy).toBeCalledTimes(1);
  });

  it("proxies mockRejectedValueOnce", () => {
    using spy = disposableSpyOn(obj, "return1Async").mockRejectedValueOnce(
      Error("3")
    );
    expect(obj.return1Async()).rejects.toThrowError(Error("3"));
    expect(spy).toBeCalledTimes(1);
    expect(obj.return1Async()).resolves.toBe(1);
    expect(spy).toBeCalledTimes(2);
  });

  it("proxies mockReturnValue", () => {
    using spy = disposableSpyOn(obj, "return1").mockReturnValue(3);
    expect(obj.return1()).toBe(3);
    expect(spy).toBeCalledTimes(1);
  });

  it("proxies mockReturnValue", () => {
    const spy = disposableSpyOn(obj, "return1").mockReturnValue(3);
    expect(obj.return1()).toBe(3);
    expect(spy).toBeCalledTimes(1);
  });
});
